package br.com.diegokappauni.viajabessa

import android.os.Bundle
import android.os.PersistableBundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.Toast
import br.com.diegokappauni.viajabessa.R
import br.com.diegokappauni.viajabessa.adapters.AdapterPacotes
import br.com.diegokappauni.viajabessa.models.Pacote
import kotlinx.android.synthetic.main.principal.*
import retrofit2.Call
import retrofit2.Response

class Principal:AppCompatActivity() {

    var recycler: RecyclerView? = null
    private var listaPacotes:ArrayList<Pacote>? = ArrayList()
    private var progressBar:ProgressBar? = null
    lateinit var adapter:AdapterPacotes
    var swipeReflesh: SwipeRefreshLayout? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.principal)



        val searchView: SearchView = findViewById(R.id.campo_pesquisa_cidade)

        progressBar = findViewById(R.id.progressBar) as ProgressBar

        recycler = findViewById(R.id.recycler_lista_pacotes) as RecyclerView


        searchView.setOnQueryTextListener(object: SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(p0: String?): Boolean {

                return false
            }

            override fun onQueryTextChange(p0: String?): Boolean {
                filtroPacotes(p0!!)

                return false
            }
        })

        val linearLayout = LinearLayoutManager(this@Principal, LinearLayout.VERTICAL,false)
        recycler?.layoutManager = linearLayout
        val fragmento: FragmentManager? = null
        adapter =  AdapterPacotes(this@Principal,listaPacotes,fragmento)
        recycler?.adapter = adapter

        obterDados()

        swipeReflesh = findViewById(R.id.reflesh) as SwipeRefreshLayout

        swipeReflesh!!.setOnRefreshListener {
            swipeReflesh!!.post {


                obterDados()

            }

        }



    }


    private fun obterDados() {
        val call: Call<List<Pacote>> = RetrofitInitializer().getClient.list()
        call.enqueue(object : retrofit2.Callback<List<Pacote>> {


            override fun onResponse(call: Call<List<Pacote>>?, response: Response<List<Pacote>>?) {

                progressBar?.visibility = View.VISIBLE

                listaPacotes?.clear()

                listaPacotes?.addAll(response!!.body()!!)

                recycler?.adapter?.notifyDataSetChanged()

                progressBar?.visibility = View.INVISIBLE
                swipeReflesh?.isRefreshing = false
                recycler?.adapter?.notifyDataSetChanged()


            }

            override fun onFailure(call: Call<List<Pacote>>?, t: Throwable?) {
                Toast.makeText(this@Principal,t.toString(), Toast.LENGTH_LONG).show()

                Snackbar.make(
                    view_Parent,
                    "Problema de conexão",
                    Snackbar.LENGTH_INDEFINITE
                ).setAction("Reconectar") {obterDados()}.show()

                progressBar?.visibility = View.INVISIBLE
            }

        })
    }

    fun filtroPacotes(local: String) {

        val listaFiltroLocal: ArrayList<Pacote> = ArrayList()

        val listaPacotes : ArrayList<Pacote> = listaPacotes!!

        for (resultado in listaPacotes) {
            if (resultado.titulo!!.toLowerCase().contains(local.toLowerCase())) {
                listaFiltroLocal.add(resultado)
            }
        }

        // Passando a lista filtrada para a class AdapterPacotes
        adapter.listaFiltrada(listaFiltroLocal)
    }

    override fun onBackPressed() {

    }
    }


