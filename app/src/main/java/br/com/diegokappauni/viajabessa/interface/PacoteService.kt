package br.com.diegokappauni.viajabessa.`interface`

import br.com.diegokappauni.viajabessa.models.Pacote
import retrofit2.Call
import retrofit2.http.GET

interface PacoteService {

    @GET("pacotes")
        fun list(): Call<List<Pacote>>



}