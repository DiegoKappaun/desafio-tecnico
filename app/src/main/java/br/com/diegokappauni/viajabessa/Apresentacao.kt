package br.com.diegokappauni.viajabessa

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.widget.ImageView
import com.bumptech.glide.Glide

class Apresentacao : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash_screen)

        val imagem = findViewById(R.id.imagem_splash) as ImageView

            Glide.with(this)
            .load(R.drawable.viagem)
            .into(imagem)


        Handler().postDelayed(Runnable {


            startActivity(Intent(this,Principal::class.java))

        },3000)


    }


}
