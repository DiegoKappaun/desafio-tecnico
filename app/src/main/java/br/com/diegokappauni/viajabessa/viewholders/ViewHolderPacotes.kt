package br.com.diegokappauni.viajabessa.viewholders

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import br.com.diegokappauni.viajabessa.R
import br.com.diegokappauni.viajabessa.models.Pacote
import kotlinx.android.synthetic.main.card_view_pacotes.view.*

class ViewHolderPacotes(itemView: View) : RecyclerView.ViewHolder(itemView)
