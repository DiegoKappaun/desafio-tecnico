package br.com.diegokappauni.viajabessa.adapters



import android.content.Context
import android.content.Intent
import android.graphics.Paint
import android.support.v4.app.FragmentManager
import android.support.v7.app.AlertDialog
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import br.com.diegokappauni.viajabessa.Reserva
//import br.com.diegokappauni.viajabessa.FragmentDetalhes
import br.com.diegokappauni.viajabessa.R
import br.com.diegokappauni.viajabessa.models.Pacote
import br.com.diegokappauni.viajabessa.viewholders.ViewHolderPacotes
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.card_view_pacotes.view.*
import kotlinx.android.synthetic.main.detalhes.view.*

class AdapterPacotes(private val context:Context?,var pacotes: ArrayList<Pacote>?,var fragmentoManager: FragmentManager?): RecyclerView.Adapter<RecyclerView.ViewHolder>() {



    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): RecyclerView.ViewHolder {

        return ViewHolderPacotes(LayoutInflater.from(context).inflate(R.layout.card_view_pacotes,p0,false))

    }

    override fun getItemCount(): Int {

        return pacotes!!.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {


       //Card Principal
       val pacoteModel = pacotes?.get(position)
        holder.itemView.titulo_lugar_card.text = pacoteModel?.titulo
        holder.itemView.dias_hotel.text = pacoteModel?.dias_hotel
        holder.itemView.preco_card.text = pacoteModel?.preco
        holder.itemView.preco_desconto.text = pacoteModel?.desconto
        holder.itemView.preco_card.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG


             Glide.with(context!!)
            .load(pacoteModel?.imagem)
            .into(holder.itemView.imagem_card)


            holder.itemView.setOnClickListener {

                //Tela detalhes sendo inflada e sendo exibida em um AlertDialog customizado
                val view: View = LayoutInflater.from(context).inflate(R.layout.detalhes,null)

                val imagem_hotel = view.imagem_hotel as ImageView
                val nome_hotel = view.txt_nome_hotel as TextView
                val local_hotel = view.txt_local_hotel as TextView
                val descricao_hotel = view.txt_descricao_hotel as TextView
                val imagem_quarto = view.imagem_quarto as ImageView
                val btn_comprar = view.btn_comprar as Button


                Glide.with(context)
                    .load(pacoteModel?.imagem_hotel)
                    .into(imagem_hotel)

                nome_hotel.setText(pacoteModel?.nome_hotel)
                local_hotel.setText(pacoteModel?.local_hotel)
                descricao_hotel.setText(pacoteModel?.descricao_hotel)

                Glide.with(context)
                    .load(pacoteModel?.imagem_quarto)
                    .into(imagem_quarto)

                btn_comprar.setOnClickListener{


               context.startActivity(Intent(context,Reserva::class.java))

                }


                AlertDialog.Builder(context)
                    .setView(view)
                    .setCancelable(true)
                    .show()


            }


    }


    fun listaFiltrada(filteredCourseList: ArrayList<Pacote>) {
        this.pacotes = filteredCourseList
        notifyDataSetChanged()
    }
}