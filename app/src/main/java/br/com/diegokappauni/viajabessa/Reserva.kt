package br.com.diegokappauni.viajabessa

import android.content.DialogInterface
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import android.widget.EditText
import android.widget.Toast


class Reserva : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.tela_reserva)

        val edt_nome = findViewById(R.id.edit_nome) as EditText
        val edt_sobrenome = findViewById(R.id.edit_sobrenome) as EditText
        val edt_telefone = findViewById(R.id.edit_telefone) as EditText
        val edt_email = findViewById(R.id.edit_email) as EditText
        val btn_reserva = findViewById(R.id.btn_reserva) as Button



        btn_reserva.setOnClickListener{


            val nome = edt_nome.text
            val sobrenome = edt_sobrenome.text
            val telefone = edt_telefone.text
            val email = edt_email.text

            if((nome.isNullOrEmpty()||
                        (sobrenome.isNullOrEmpty())||
                        (telefone.isNullOrEmpty()))||
                        (email.isNullOrEmpty())){

                Toast.makeText(this,"Digite todos seus dados",Toast.LENGTH_LONG).show()

            }else {


                AlertDialog.Builder(this)
                    .setTitle("Confirmaçāo de reserva")
                    .setMessage("Deseja confirmar reserva com os dados abaixo? \n \n nome: $nome \n sobrenome: $sobrenome \n telefone: $telefone \n e-mail: $email")
                    .setPositiveButton(
                        "Confirmar",
                        DialogInterface.OnClickListener { dialog, which ->
                            Toast.makeText(
                                this,
                                "Pacote reservado com sucesso !",
                                Toast.LENGTH_LONG
                            ).show()

                            onBackPressed()

                        })
                    .setNegativeButton("Cancelar", DialogInterface.OnClickListener { dialog, which -> onBackPressed() })
                    .show()


            }
        }


    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

}
