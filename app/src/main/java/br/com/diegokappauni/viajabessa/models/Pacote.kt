package br.com.diegokappauni.viajabessa.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Pacote(@Expose @SerializedName("id") val id:Int?,
             @Expose @SerializedName("titulo") val titulo:String?,
             @Expose @SerializedName("dias_hotel") val dias_hotel:String?,
             @Expose @SerializedName("preco") val preco:String?,
             @Expose @SerializedName("desconto") val desconto:String?,
             @Expose @SerializedName("imagem") val imagem:String?,
             @Expose @SerializedName("imagem_hotel") val imagem_hotel:String?,
             @Expose @SerializedName("nome_hotel") val nome_hotel:String?,
             @Expose @SerializedName("local_hotel") val local_hotel:String,
             @Expose @SerializedName("descricao_hotel") val descricao_hotel:String?,
             @Expose @SerializedName("imagem_quarto") val imagem_quarto:String?)







